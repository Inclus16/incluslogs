<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\LogTagColorController;
use App\Http\Controllers\LogTagController;
use App\Http\Controllers\LogTagTargetWordController;
use App\Http\Controllers\UserController;
use App\Models\Role;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'token'], function () {
    Route::get('get', [AuthController::class, 'getToken'])->name('token.get');
    Route::get('refresh', [AuthController::class, 'refreshToken'])->name('token.refresh');
});
Route::group(['middleware' => 'auth:api'], function () {
    Route::group(['prefix' => 'log'], function () {
        Route::group(['prefix' => 'tag'], function () {
            Route::get('/', [LogTagController::class, 'list'])->name('log.tag.get');
            Route::group(['middleware' => 'role:' . Role::ADMINISTRATOR_ID], function () {
                Route::put('/{tag}', [LogTagController::class, 'update'])->where('tag', '^\d+$')->name('log.tag.put');
                Route::delete('/{tag}', [LogTagController::class, 'remove'])->where('tag', '^\d+$')->name('log.tag.delete');
                Route::post('/', [LogTagController::class, 'add'])->name('log.tag.post');
                Route::get('/tree', [LogTagController::class, 'tree'])->name('log.tag.get.tree');
            });
        });
        Route::group(['prefix' => 'color'], function () {
            Route::get('/', [LogTagColorController::class, 'list'])->name('log.tag.color.get');
            Route::group(['middleware' => 'role:' . Role::ADMINISTRATOR_ID], function () {
                Route::put('/{tagColor}', [LogTagColorController::class, 'update'])->where('tagColor', '^\d+$')->name('log.tag.color.put');
                Route::delete('/{tagColor}', [LogTagColorController::class, 'remove'])->where('tagColor', '^\d+$')->name('log.tag.color.delete');
                Route::post('/', [LogTagColorController::class, 'add'])->name('log.tag.color.post');
            });
        });
        Route::group(['prefix' => 'word'], function () {
            Route::get('/', [LogTagTargetWordController::class, 'list'])->name('log.tag.word.get');
            Route::group(['middleware' => 'role:' . Role::ADMINISTRATOR_ID], function () {
                Route::put('/{word}', [LogTagTargetWordController::class, 'update'])->where('word', '^\d+$')->name('log.tag.word.put');
                Route::delete('/{word}', [LogTagTargetWordController::class, 'remove'])->where('word', '^\d+$')->name('log.tag.word.delete');
                Route::post('/', [LogTagTargetWordController::class, 'add'])->name('log.tag.word.post');
            });
        });
        Route::group(['prefix' => 'log'], function () {
            Route::get('/', [LogController::class, 'search'])->name('log.search.get');
            Route::group(['middleware' => 'role:' . Role::ADMINISTRATOR_ID], function () {
                Route::delete('/{log}', [LogController::class, 'delete'])->name('log.delete');
            });
        });
    });
    Route::group(['prefix' => 'user', 'middleware' => 'role:' . Role::ADMINISTRATOR_ID], function () {
        Route::get('/', [UserController::class, 'list'])->name('user.get');
        Route::put('/{user}', [UserController::class, 'update'])->where('user', '^\d+$')->name('user.put');
        Route::delete('/{user}', [UserController::class, 'remove'])->where('user', '^\d+$')->name('user.delete');
        Route::post('/', [UserController::class, 'add'])->name('user.post');
        Route::patch('/{user}', [UserController::class, 'changePassword'])->where('user', '^\d+$')->name('user.patch');
    });
});
