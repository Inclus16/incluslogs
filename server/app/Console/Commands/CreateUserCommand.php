<?php

namespace App\Console\Commands;

use App\Dto\UserDtoWithPassword;
use App\Models\Role;
use Illuminate\Console\Command;
use App\Services\UserService;
use Illuminate\Support\Facades\Validator;

class CreateUserCommand extends Command
{
    private UserService $userService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create {name} {login} {password} {role_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates new user';

    /**
     * Create a new command instance.
     *
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $arguments = $this->arguments();
        $validatorInstance = Validator::make($arguments, UserService::$addValidationRules);
        if ($validatorInstance->fails()) {
            foreach ($validatorInstance->errors()->toArray() as $argument => $error) {
                $this->error("{$argument}: {$error[0]}");
            }
            return 1;
        }
        $this->userService->createUser(new UserDtoWithPassword($arguments['name'], $arguments['login'], $arguments['password'], Role::ADMINISTRATOR_ID));
        $this->info("Administrator with login: {$arguments['login']} and password: {$arguments['password']} successfully created!");
        return 0;
    }
}
