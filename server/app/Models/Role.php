<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $description
 */
class Role extends Model
{
    use HasFactory;

    const ADMINISTRATOR_ID = 1;

    const WORKER_ID = 2;

    public $timestamps = false;

    protected $fillable = ['id', 'name', 'description'];
}
