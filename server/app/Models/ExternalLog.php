<?php

namespace App\Models;

use App\Dto\LogSearchDto;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $body
 * @property int $tag_id
 * @property LogTag $tag
 * @property string $created_at
 * @property string $updated_at
 * @method static Builder searchViaDto(LogSearchDto $dto)
 */
class ExternalLog extends Model
{
    use HasFactory;

    public function tag()
    {
        return $this->hasOne(LogTag::class, 'id', 'tag_id');
    }

    public function scopeSearchViaDto(Builder $builder, LogSearchDto $dto)
    {
        if ($dto->isDatePresent()) {
            $builder->whereBetween('created_at', [$dto->startDate, $dto->endDate]);
        }
        if ($dto->isTagIdPresent()) {
            $builder->where(['tag_id' => $dto->tagId]);
        }
        if ($dto->isWordPresent()) {
            $builder->where('body', 'LIKE', "%{$dto->word}%");
        }
        return $builder;
    }
}
