<?php

namespace App\Models;

use App\Dto\LogTagTargetWordDto;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $words
 * @property int $tag_id
 */
class LogTagTargetWord extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ['words', 'tag_id'];

    public static function createFromDto(LogTagTargetWordDto $dto): self
    {
        return new self(['words' => $dto->getWords(), 'tag_id' => $dto->getTagId()]);
    }

    public function fillByDto(LogTagTargetWordDto $dto): self
    {
        $this->tag_id = $dto->getTagId();
        $this->words = $dto->getWords();
        return $this;
    }
}
