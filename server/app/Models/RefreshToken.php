<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $refresh_token
 * @property int $user_id
 * @property User $user
 * @method static void deleteByUserId(int $userId)
 * @method static Builder findByRefreshToken(string $refreshToken)
 */
class RefreshToken extends Model
{
    use HasFactory;

    protected $table = 'refresh_tokens';

    public $timestamps = false;

    protected $primaryKey = 'id';

    protected $fillable = [
        'refresh_token',
        'user_id'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function scopeDeleteByUserId(Builder $builder, int $arg)
    {
        return $builder->where(['user_id' => $arg])->delete();
    }

    public function scopeFindByRefreshToken(Builder $builder, string $arg)
    {
        return $builder->where(['refresh_token' => $arg]);
    }
}
