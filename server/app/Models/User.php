<?php

namespace App\Models;

use App\Dto\UserDto;
use App\Dto\UserDtoWithPassword;
use Ds\Map;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * @property int $id
 * @property string $name
 * @property string $login
 * @property string $password
 * @property int $role_id
 * @property Role $role
 * @method static Builder firstByLogin(string $login)
 * @method static Builder workers()
 * @method static Builder administrators()
 */
class User extends Authenticatable
{
    use HasFactory, Notifiable;

    public const PRIMARY_KEY_NAME = 'id';

    const TABLE = 'users';

    /**
     * @var string[]
     */
    protected $fillable = [
        'login',
        'name',
        'password',
        'role_id'
    ];

    /**
     * @var string[]
     */
    protected $hidden = [
        'password'
    ];

    public function scopeFirstByLogin(Builder $builder, string $login)
    {
        return $builder->where(['login' => $login]);
    }

    public function role()
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }

    public function scopeWorkers(Builder $builder)
    {
        return $builder->where(['role_id' => Role::WORKER_ID]);
    }

    public function scopeAdministrators(Builder $builder)
    {
        return $builder->where(['role_id' => Role::ADMINISTRATOR_ID]);
    }

    public static function createFromDto(UserDtoWithPassword $dto): self
    {
        return new self([
            'login' => $dto->getLogin(),
            'password' => $dto->getPassword(),
            'name' => $dto->getName(),
            'role_id' => $dto->getRoleId()
        ]);
    }

    public function getAuthData(): Map
    {
        return new Map([
            'name' => $this->name,
            'id' => $this->id
        ]);
    }

    public function fillByDto(UserDto $dto): self
    {
        $this->login = $dto->getLogin();
        $this->name = $dto->getName();
        $this->role_id = $dto->getRoleId();
        return $this;
    }

    public function isAdmin(): bool
    {
        return $this->role_id === Role::ADMINISTRATOR_ID;
    }

    public function hasRole(int $roleId): bool
    {
        return $this->role_id === $roleId;
    }
}
