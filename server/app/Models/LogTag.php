<?php

namespace App\Models;

use App\Dto\LogTagDto;
use App\Dto\LogTagTreeItemDto;
use Ds\Vector;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\DB;

/**
 * @property int $id
 * @property int $parent_id
 * @property int $color_id
 * @property string $name
 * @property LogTagColor $color
 * @property LogTagTargetWord[]|Collection $words
 * @property LogTag $parent
 * @property ExternalLog[]|Collection $logs
 * @method static Builder withColor()
 * @method static Builder withWords()
 * @method static Builder upperMost()
 * @method static Builder withLogs()
 */
class LogTag extends Model
{
    use HasFactory;

    public const TABLE = 'log_tags';

    protected $table = self::TABLE;

    public $timestamps = false;

    protected $fillable = ['name', 'color_id', 'parent_id'];

    public function color(): HasOne
    {
        return $this->hasOne(LogTagColor::class, 'id', 'color_id');
    }

    public function words()
    {
        return $this->hasMany(LogTagTargetWord::class, 'tag_id', 'id');
    }

    public function parent()
    {
        return $this->hasOne(LogTag::class, 'id', 'parent_id');
    }

    public function logs()
    {
        return $this->hasMany(ExternalLog::class, 'tag_id', 'id');
    }

    public static function createFromDto(LogTagDto $dto): self
    {
        return new self(['name' => $dto->getName(), 'color_id' => $dto->getColorId(), 'parent_id' => $dto->getParentId()]);
    }

    public function fillFromDto(LogTagDto $dto): self
    {
        $this->name = $dto->getName();
        $this->color_id = $dto->getColorId();
        $this->parent_id = $dto->getParentId();
        return $this;
    }

    public function scopeWithColor(Builder $builder)
    {
        return $builder->with('color');
    }

    public function scopeWithWords(Builder $builder)
    {
        return $builder->with('words');
    }

    public function scopeWithLogs(Builder $builder)
    {
        return $builder->with('logs');
    }

    public function scopeUpperMost(Builder $builder)
    {
        return $builder->where(['parent_id' => null])->limit(1);
    }

    /**
     * @return Vector|\stdClass[]
     */
    public function getTree(): Vector
    {
        $records = new Vector(DB::select('WITH RECURSIVE total(id,name,color_id,parent_id)
        AS (SELECT lt.id,lt.name,lt.color_id,lt.parent_id FROM log_tags AS lt WHERE id=:id
    UNION ALL SELECT lt2.id,lt2.name,lt2.color_id,lt2.parent_id FROM log_tags AS lt2,total AS tt WHERE tt.id=lt2.parent_id
    ) SELECT t.id AS id, t.name AS name, t.color_id AS color_id, t.parent_id AS parent_id, c.color AS color
    FROM total AS t JOIN log_tag_colors AS c ON t.color_id=c.id ORDER BY parent_id', [':id' => $this->id]));
        $records->sort(function ($a, $b) {
            return ($a > $b) ? 1 : -1;
        });
        return $records;
    }
}
