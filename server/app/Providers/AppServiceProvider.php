<?php

namespace App\Providers;

use App\Services\Security\Abstractions\HashServiceInterface;
use App\Services\Security\BcryptHashService;
use App\Services\Security\JwtGuard;
use App\Services\Security\JwtService;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(HashServiceInterface::class, BcryptHashService::class);
        $this->app->singleton(JwtService::class);
        $this->app->bind(UserProvider::class, function () {
            return Auth::createUserProvider(config('auth.guards.api.provider'));
        });
        $this->app->singleton(Guard::class, JwtGuard::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
