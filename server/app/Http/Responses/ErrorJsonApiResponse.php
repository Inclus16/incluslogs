<?php


namespace App\Http\Responses;

use App\Http\Responses\Abstractions\AbstractJsonApiResponse;
use App\Http\Responses\Abstractions\JsonApiResponseInterface;
use Ds\Collection;
use Ds\Map;
use Ds\Sequence;

class ErrorJsonApiResponse extends AbstractJsonApiResponse
{
    protected bool $success = false;

    protected string $dataKey = 'errors';

    public static function createEmpty(int $statusCode = 500, Map $headers = null): self
    {
        return (new self(null, $statusCode, $headers));
    }

    public static function createFromCollection(Collection $errors, int $statusCode = 500, Map $headers = null): self
    {
        return (new self($errors, $statusCode, $headers));
    }
}
