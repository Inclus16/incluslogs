<?php


namespace App\Http\Responses;

use App\Http\Responses\Abstractions\AbstractJsonApiResponse;
use Ds\Collection;
use Ds\Map;
use Ds\Sequence;

class SuccessJsonApiResponse extends AbstractJsonApiResponse
{
    protected bool $success = true;

    protected string $dataKey = 'data';

    public static function createEmpty(int $statusCode = 200, Map $headers = null): self
    {
        return (new self(null, $statusCode, $headers));
    }

    public static function createFromCollection(Collection $data, int $statusCode = 200, Map $headers = null): self
    {
        return (new self($data, $statusCode, $headers));
    }
}
