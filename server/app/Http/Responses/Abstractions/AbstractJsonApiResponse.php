<?php


namespace App\Http\Responses\Abstractions;

use Ds\Collection;
use Ds\Map;
use Ds\Sequence;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractJsonApiResponse extends Response
{
    private const DEFAULT_HEADER_KEY = 'Content-Type';

    private const DEFAULT_HEADER_VALUE = 'application/json';

    protected bool $success;

    protected ?Map $meta;

    protected ?Collection $data;

    protected string $dataKey;

    public function __construct(Collection $data = null, int $status = Response::HTTP_OK, Map $headers = null)
    {
        $this->meta = new Map(['code' => $status]);
        $this->data = $data;
        parent::__construct($this->getJsonResponseAsString(), Response::HTTP_OK, $this->addDefaultHeaderIfNotPresent($headers)->toArray());
    }

    private function addDefaultHeaderIfNotPresent(?Map $headers): Map
    {
        if (null === $headers) {
            $headers = new Map();
        }
        if ($headers->isEmpty() || !$headers->hasKey(self::DEFAULT_HEADER_KEY)) {
            $headers->put(self::DEFAULT_HEADER_KEY, self::DEFAULT_HEADER_VALUE);
        }
        return $headers;
    }

    private function getJsonResponseAsString()
    {
        return json_encode(['success' => $this->success, 'meta' => $this->meta, $this->dataKey => $this->data ?? []]);
    }
}
