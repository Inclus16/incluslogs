<?php

namespace App\Http\Middleware;

use App\Http\Responses\ErrorJsonApiResponse;
use Closure;
use Symfony\Component\HttpFoundation\Response;

class RoleAuthorization
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (!$request->user()->hasRole($role)) {
            return ErrorJsonApiResponse::createEmpty(Response::HTTP_FORBIDDEN);
        }

        return $next($request);
    }
}
