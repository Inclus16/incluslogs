<?php

namespace App\Http\Middleware;

use App\Http\Responses\ErrorJsonApiResponse;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Auth\Factory as Auth;

class Authenticate
{
    /**
     * The authentication factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param \Illuminate\Contracts\Auth\Factory $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string $guard
     * @return mixed
     *
     */
    public function handle($request, Closure $next, string $guard)
    {
        if ($this->auth->guard($guard)->check()) {
            $this->auth->shouldUse($guard);
            $this->auth->guard($guard)->persistUser();
            return $next($request);
        } else {
            return ErrorJsonApiResponse::createEmpty(401);
        }
    }
}
