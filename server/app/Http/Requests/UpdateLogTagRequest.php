<?php

namespace App\Http\Requests;

use App\Dto\LogTagDto;
use App\Http\Requests\Abstractions\AbstractApiRequest;
use App\Models\LogTag;
use App\Models\LogTagColor;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class UpdateLogTagRequest extends AbstractApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function getLogTagDto(): LogTagDto
    {
        $data = $this->validated();
        return new LogTagDto($data['name'], $data['color_id'], $data['parent_id']);
    }

    protected function passedValidation()
    {
        $data = $this->validated();
        $parentId = $data['parent_id'];
        $logTag = $this->getLogTag();
        $child = $logTag->getTree();
        if (!$child->filter(function (\stdClass $data) use ($parentId) {
            return $data->id === $parentId;
        })->isEmpty()) {
            $this->addValidationError('parent_id', 'Recursive hierarchy error');
            $this->failedValidation($this->validator);
        }
    }

    private function getLogTag(): LogTag
    {
        return $this->route('tag');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'color_id' => 'required|integer|exists:' . LogTagColor::class . ',id',
            'name' => ['required', Rule::unique(LogTag::TABLE, 'name')->ignore($this->getLogTag()->id)],
            'parent_id' => 'nullable|integer|exists:' . LogTag::class . ',id'
        ];
    }
}
