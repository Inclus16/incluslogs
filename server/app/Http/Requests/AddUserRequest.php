<?php

namespace App\Http\Requests;

use App\Dto\UserDtoWithPassword;
use App\Http\Requests\Abstractions\AbstractApiRequest;
use App\Services\UserService;

class AddUserRequest extends AbstractApiRequest
{
    public function getDto(): UserDtoWithPassword
    {
        $data = $this->validated();
        return new UserDtoWithPassword($data['name'], $data['login'], $data['password'], $data['role_id']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return UserService::$addValidationRules;
    }
}
