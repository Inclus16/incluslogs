<?php

namespace App\Http\Requests;

use App\Http\Requests\Abstractions\AbstractApiRequest;
use App\Models\LogTagColor;
use Illuminate\Foundation\Http\FormRequest;

class AddLogTagColorRequest extends AbstractApiRequest
{
    public function getColor(): string
    {
        return $this->validated()['color'];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'color' => 'required|unique:' . LogTagColor::class . ',color'
        ];
    }
}
