<?php

namespace App\Http\Requests;

use App\Dto\LogSearchDto;
use App\Http\Requests\Abstractions\AbstractApiRequest;
use App\Models\LogTag;
use Illuminate\Foundation\Http\FormRequest;

class SearchLogsRequest extends AbstractApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function getDto(): LogSearchDto
    {
        return new LogSearchDto(
            $this->request->get('date_start'),
            $this->request->get('date_end'),
            $this->request->get('tag_id'),
            $this->request->get('word')
        );
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tag_id' => 'nullable|integer|exists:' . LogTag::class . ',id',
            'date_start' => 'nullable',
            'date_end' => 'nullable',
            'word' => 'nullable'
        ];
    }
}
