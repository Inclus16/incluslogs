<?php

namespace App\Http\Requests;

use App\Dto\LogTagTargetWordDto;
use App\Http\Requests\Abstractions\AbstractApiRequest;
use App\Models\LogTag;
use Illuminate\Foundation\Http\FormRequest;

class AddLogTagTargetWordRequest extends AbstractApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function getDto(): LogTagTargetWordDto
    {
        $data = $this->validated();
        return new LogTagTargetWordDto($data['words'], $data['tag_id']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tag_id' => 'required|exists:' . LogTag::class . ',id',
            'words' => 'required'
        ];
    }
}
