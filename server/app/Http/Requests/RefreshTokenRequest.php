<?php

namespace App\Http\Requests;

use App\Http\Requests\Abstractions\AbstractApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class RefreshTokenRequest extends AbstractApiRequest
{
    public function getRefreshTokenString(): string
    {
        return $this->validated()['refresh_token'];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'refresh_token' => 'required'
        ];
    }
}
