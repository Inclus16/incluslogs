<?php

namespace App\Http\Requests;

use App\Dto\UserDto;
use App\Http\Requests\Abstractions\AbstractApiRequest;
use App\Models\Role;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends AbstractApiRequest
{
    private function getUserRouteParam(): User
    {
        return $this->route('user');
    }

    public function getDto(): UserDto
    {
        $data = $this->validated();
        return new UserDto($data['name'], $data['login'], $data['role_id']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100',
            'login' => ['required', 'max:100',
                Rule::unique(User::TABLE, 'login')->ignore($this->getUserRouteParam()->id)],
            'role_id' => 'required|exists:' . Role::class . ',id'
        ];
    }
}
