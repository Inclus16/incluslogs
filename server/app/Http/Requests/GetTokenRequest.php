<?php

namespace App\Http\Requests;

use App\Dto\CredentialsDto;
use App\Http\Requests\Abstractions\AbstractApiRequest;

class GetTokenRequest extends AbstractApiRequest
{
    public function getCredentials(): CredentialsDto
    {
        $validated = $this->validated();
        return new CredentialsDto($validated['login'], $validated['password']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login' => 'required',
            'password' => 'required'
        ];
    }
}
