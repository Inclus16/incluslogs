<?php

namespace App\Http\Requests;

use App\Http\Requests\Abstractions\AbstractApiRequest;

class ChangePasswordRequest extends AbstractApiRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'new_password' => 'required'
        ];
    }

    public function getNewPassword()
    {
        return $this->validated()['new_password'];
    }
}
