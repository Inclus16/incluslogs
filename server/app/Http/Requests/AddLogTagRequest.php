<?php

namespace App\Http\Requests;

use App\Dto\LogTagDto;
use App\Http\Requests\Abstractions\AbstractApiRequest;
use App\Models\LogTag;
use App\Models\LogTagColor;

class AddLogTagRequest extends AbstractApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function getLogTagDto(): LogTagDto
    {
        $data = $this->validated();
        return new LogTagDto($data['name'], $data['color_id'], $data['parent_id']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'color_id' => 'required|integer|exists:' . LogTagColor::class . ',id',
            'name' => 'required|unique:' . LogTag::class . ',name',
            'parent_id' => 'nullable|integer|in:' . LogTag::class . ',id'
        ];
    }
}
