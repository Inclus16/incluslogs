<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddLogTagColorRequest;
use App\Http\Responses\Abstractions\AbstractJsonApiResponse;
use App\Http\Responses\SuccessJsonApiResponse;
use App\Models\LogTagColor;
use App\Services\Logs\LogTagColorService;

class LogTagColorController extends Controller
{
    private LogTagColorService $tagColorService;

    public function __construct(LogTagColorService $tagColorService)
    {
        $this->tagColorService = $tagColorService;
    }

    public function add(AddLogTagColorRequest $request): AbstractJsonApiResponse
    {
        $this->tagColorService->add($request->getColor());
        return SuccessJsonApiResponse::createEmpty();
    }

    public function remove(LogTagColor $tagColor)
    {
        $this->tagColorService->remove($tagColor);
        return SuccessJsonApiResponse::createEmpty();
    }

    public function list()
    {
        return SuccessJsonApiResponse::createFromCollection($this->tagColorService->list());
    }

    public function update(LogTagColor $tagColor, AddLogTagColorRequest $request)
    {
        $this->tagColorService->update($tagColor, $request->getColor());
        return SuccessJsonApiResponse::createEmpty();
    }
}
