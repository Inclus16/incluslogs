<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddUserRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Responses\Abstractions\AbstractJsonApiResponse;
use App\Http\Responses\SuccessJsonApiResponse;
use App\Models\User;
use App\Services\UserService;

class UserController extends Controller
{
    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function list(): AbstractJsonApiResponse
    {
        return SuccessJsonApiResponse::createFromCollection($this->userService->list());
    }

    public function add(AddUserRequest $request): AbstractJsonApiResponse
    {
        $this->userService->createUser($request->getDto());
        return SuccessJsonApiResponse::createEmpty();
    }

    public function update(UpdateUserRequest $request, User $user): AbstractJsonApiResponse
    {
        $this->userService->update($user, $request->getDto());
        return SuccessJsonApiResponse::createEmpty();
    }

    public function remove(User $user):AbstractJsonApiResponse
    {
        $this->userService->remove($user);
        return SuccessJsonApiResponse::createEmpty();
    }

    public function changePassword(ChangePasswordRequest $request, User $user):AbstractJsonApiResponse
    {
        $this->userService->changePassword($user, $request->getNewPassword());
        return SuccessJsonApiResponse::createEmpty();
    }
}
