<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchLogsRequest;
use App\Http\Responses\SuccessJsonApiResponse;
use App\Models\ExternalLog;
use App\Services\Logs\LogService;

class LogController extends Controller
{
    private LogService $logService;

    public function __construct(LogService $logService)
    {
        $this->logService = $logService;
    }


    public function search(SearchLogsRequest $request)
    {
        return SuccessJsonApiResponse::createFromCollection($this->logService->search($request->getDto()));
    }

    public function delete(ExternalLog $log)
    {
        $this->logService->delete($log);
        return SuccessJsonApiResponse::createEmpty();
    }
}
