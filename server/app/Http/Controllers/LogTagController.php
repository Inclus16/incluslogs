<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddLogTagRequest;
use App\Http\Requests\UpdateLogTagRequest;
use App\Http\Responses\SuccessJsonApiResponse;
use App\Models\LogTag;
use App\Services\Logs\LogTagService;

class LogTagController extends Controller
{
    private LogTagService $logTagService;

    public function __construct(LogTagService $logTagService)
    {
        $this->logTagService = $logTagService;
    }

    public function add(AddLogTagRequest $request)
    {
        $this->logTagService->save($request->getLogTagDto());
        return SuccessJsonApiResponse::createEmpty();
    }

    public function list()
    {
        return SuccessJsonApiResponse::createFromCollection($this->logTagService->list());
    }

    public function update(UpdateLogTagRequest $request, LogTag $tag)
    {
        $this->logTagService->update($tag, $request->getLogTagDto());
        return SuccessJsonApiResponse::createEmpty();
    }

    public function remove(LogTag $tag)
    {
        $this->logTagService->remove($tag);
        return SuccessJsonApiResponse::createEmpty();
    }

    public function tree()
    {
        return SuccessJsonApiResponse::createFromCollection($this->logTagService->getTree());
    }
}
