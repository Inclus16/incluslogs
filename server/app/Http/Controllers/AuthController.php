<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetTokenRequest;
use App\Http\Requests\RefreshTokenRequest;
use App\Http\Responses\Abstractions\AbstractJsonApiResponse;
use App\Http\Responses\ErrorJsonApiResponse;
use App\Services\UserService;
use App\Http\Responses\SuccessJsonApiResponse;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function getToken(GetTokenRequest $request): AbstractJsonApiResponse
    {
        $authResult = $this->userService->authenticateUser($request->getCredentials());
        if (!$authResult->isSuccess()) {
            return ErrorJsonApiResponse::createEmpty(Response::HTTP_UNAUTHORIZED);
        }
        return SuccessJsonApiResponse::createFromCollection($authResult->toMap());
    }

    public function refreshToken(RefreshTokenRequest $request): AbstractJsonApiResponse
    {
        $authResult = $this->userService->refreshJwt($request->getRefreshTokenString());
        if (!$authResult->isSuccess()) {
            return ErrorJsonApiResponse::createEmpty(Response::HTTP_UNAUTHORIZED);
        }
        return SuccessJsonApiResponse::createFromCollection($authResult->toMap());
    }
}
