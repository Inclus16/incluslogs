<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddLogTagTargetWordRequest;
use App\Http\Responses\Abstractions\AbstractJsonApiResponse;
use App\Http\Responses\SuccessJsonApiResponse;
use App\Models\LogTagTargetWord;
use App\Services\Logs\LogTagTargetWordService;
use Illuminate\Http\Request;

class LogTagTargetWordController extends Controller
{
    private LogTagTargetWordService $logTagTargetWordService;


    public function __construct(LogTagTargetWordService $logTagTargetWordService)
    {
        $this->logTagTargetWordService = $logTagTargetWordService;
    }


    public function list(): AbstractJsonApiResponse
    {
        return SuccessJsonApiResponse::createFromCollection($this->logTagTargetWordService->list());
    }

    public function add(AddLogTagTargetWordRequest $request)
    {
        $this->logTagTargetWordService->add($request->getDto());
        return SuccessJsonApiResponse::createEmpty();
    }

    public function remove(LogTagTargetWord $word)
    {
        $this->logTagTargetWordService->remove($word);
        return SuccessJsonApiResponse::createEmpty();
    }

    public function update(AddLogTagTargetWordRequest $request, LogTagTargetWord $word)
    {
        $this->logTagTargetWordService->update($request->getDto(), $word);
        return SuccessJsonApiResponse::createEmpty();
    }
}
