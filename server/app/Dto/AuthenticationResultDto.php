<?php


namespace App\Dto;

use Ds\Collection;
use Ds\Map;

class AuthenticationResultDto
{
    private bool $success;

    private ?string $token;

    private ?string $refreshToken;


    private function __construct(bool $success, string $token = null, string $refreshToken = null)
    {
        $this->success = $success;
        $this->token = $token;
        $this->refreshToken = $refreshToken;
    }

    public static function createErrorResult(): self
    {
        return new self(false, null, null);
    }

    public static function createSuccessResult(JwtTokensDto $tokens): self
    {
        return new self(true, $tokens->getToken(), $tokens->getRefreshToken());
    }


    public function isSuccess(): bool
    {
        return $this->success;
    }

    public function toMap(): Collection
    {
        return new Map([
            'token' => $this->token,
            'refresh_token' => $this->refreshToken
        ]);
    }
}
