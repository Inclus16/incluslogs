<?php


namespace App\Dto;

use Ds\Vector;

class LogTagDto
{
    private string $name;

    private int $colorId;

    private ?int $parentId;


    public function __construct(string $name, int $colorId, ?int $parentId)
    {
        $this->name = $name;
        $this->colorId = $colorId;
        $this->parentId = $parentId;
    }


    public function getName(): string
    {
        return $this->name;
    }


    public function getColorId(): int
    {
        return $this->colorId;
    }

    public function getParentId(): ?int
    {
        return $this->parentId;
    }
}
