<?php


namespace App\Dto;

class UserDto
{
    private string $name;

    private string $login;

    private int $roleId;


    public function __construct(string $name, string $login, int $roleId)
    {
        $this->name = $name;
        $this->login = $login;
        $this->roleId = $roleId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLogin(): string
    {
        return $this->login;
    }
    public function getRoleId(): int
    {
        return $this->roleId;
    }
}
