<?php


namespace App\Dto;

class LogSearchDto
{
    public ?string $startDate;

    public ?string $endDate;

    public ?int $tagId;

    public ?string $word;


    public function __construct(?string $startDate, ?string $endDate, ?int $tagId, ?string $word)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->tagId = $tagId;
        $this->word = $word;
    }

    public function isDatePresent(): bool
    {
        return !is_null($this->startDate);
    }

    public function isTagIdPresent(): bool
    {
        return !is_null($this->tagId);
    }

    public function isWordPresent(): bool
    {
        return !is_null($this->word);
    }
}
