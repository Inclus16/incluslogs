<?php


namespace App\Dto;

use App\Models\LogTag;
use App\Models\LogTagColor;
use Ds\Vector;

class LogTagTreeItemDto implements \JsonSerializable
{
    private int $id;

    private ?int $parentId;

    private string $name;

    private array $color;

    private int $colorId;

    private Vector $child;

    public function __construct(int $id, int $parentId = null, string $name, array $color, int $colorId)
    {
        $this->id = $id;
        $this->parentId = $parentId;
        $this->name = $name;
        $this->color = $color;
        $this->colorId = $colorId;
        $this->child = new Vector();
    }

    public function appendChild(self $dto): self
    {
        $this->child->push($dto);
        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'parent_id' => $this->parentId,
            'name' => $this->name,
            'color_id' => $this->colorId,
            'color' => $this->color,
            'child' => $this->child
        ];
    }


    public function getId(): int
    {
        return $this->id;
    }


    public function getParentId(): ?int
    {
        return $this->parentId;
    }


    public function getName(): string
    {
        return $this->name;
    }


    public function getColor(): array
    {
        return $this->color;
    }


    public function getColorId(): int
    {
        return $this->colorId;
    }


    public function getChild(): Vector
    {
        return $this->child;
    }
}
