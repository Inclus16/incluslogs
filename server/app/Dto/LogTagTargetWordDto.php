<?php


namespace App\Dto;

class LogTagTargetWordDto
{
    private string $words;

    private int $tagId;


    public function __construct(string $words, int $tagId)
    {
        $this->words = $words;
        $this->tagId = $tagId;
    }


    public function getWords(): string
    {
        return $this->words;
    }


    public function getTagId(): int
    {
        return $this->tagId;
    }
}
