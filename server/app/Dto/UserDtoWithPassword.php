<?php


namespace App\Dto;

class UserDtoWithPassword extends UserDto
{
    private string $password;

    public function __construct(string $name, string $login, string $password, int $roleId)
    {
        parent::__construct($name, $login, $roleId);
        $this->password = $password;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function changePassword(string $newPassword): self
    {
        $this->password = $newPassword;
        return $this;
    }
}
