<?php


namespace App\Services\Security\Abstractions;

interface HashServiceInterface
{
    public function hashString(string $string): string;

    public function verifyString(string $plain, string $hashed): bool;
}
