<?php


namespace App\Services\Security;

use App\Dto\JwtTokensDto;
use App\Models\RefreshToken;
use Ds\Map;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Illuminate\Support\Carbon;
use UnexpectedValueException;

class JwtService
{
    private const ISSUED_AT_KEY = 'iat';

    private const EXPIRED_AT = 'exp';

    public function makeTokens(Map $data, int $userId): JwtTokensDto
    {
        $refreshTokenString = $this->createRefreshTokenString();
        $this->saveRefreshToken($refreshTokenString, $userId);
        ;
        return new JwtTokensDto(JWT::encode($this->getPayloadFromArray($data), config('auth.jwt_secret')), $refreshTokenString);
    }

    public function getRefreshToken(string $refreshTokenString): ?RefreshToken
    {
        return RefreshToken::findByRefreshToken($refreshTokenString)->first();
    }

    private function saveRefreshToken(string $refreshTokenString, int $userId): void
    {
        RefreshToken::deleteByUserId($userId);
        (new RefreshToken(['refresh_token' => $refreshTokenString, 'user_id' => $userId]))->save();
    }

    public function verify(string $jwt): bool
    {
        try {
            JWT::decode($jwt, config('auth.jwt_secret'), [config('auth.jwt_encryption_algorithm')]);
            return true;
        } catch (\DomainException|ExpiredException|UnexpectedValueException $e) {
            return false;
        }
    }

    public function getPayload(string $jwt): object
    {
        return JWT::decode($jwt, config('auth.jwt_secret'), [config('auth.jwt_encryption_algorithm')]);
    }

    private function createRefreshTokenString(): string
    {
        return uniqid('_', true);
    }

    private function getPayloadFromArray(Map $data)
    {
        return $this->getDefaultPayload()->merge($data);
    }

    private function getDefaultPayload(): Map
    {
        $now = Carbon::now();
        return new Map([
            self::ISSUED_AT_KEY => $now->timestamp,
            self::EXPIRED_AT => $now->addMinutes(20)->timestamp
        ]);
    }
}
