<?php


namespace App\Services\Security;

use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Http\Request;

class JwtGuard implements Guard
{
    private const JWT_HEADER = 'Authorization';

    private const JWT_BEARER_NAME = 'Bearer';

    private JwtService $jwtService;

    private Request $request;

    private Authenticatable $user;

    private UserProvider $userProvider;

    public function __construct(JwtService $jwtService, Request $request, UserProvider $userProvider)
    {
        $this->userProvider = $userProvider;
        $this->jwtService = $jwtService;
        $this->request = $request;
    }

    public function persistUser()
    {
        $id = User::PRIMARY_KEY_NAME;
        $this->setUser(User::find($this->getPayload()->$id));
    }

    private function getPayload(): ?object
    {
        return $this->jwtService->getPayload($this->getPureJwt());
    }


    public function check()
    {
        return $this->validateHeader() && $this->verify();
    }

    private function verify(): bool
    {
        return $this->jwtService->verify($this->getPureJwt());
    }

    private function validateHeader(): bool
    {
        $header = $this->request->hasHeader(self::JWT_HEADER);
        return null !== $header && 0 === strpos($this->getJwtHeader(), self::JWT_BEARER_NAME);
    }

    private function getJwtHeader(): string
    {
        return $this->request->header(self::JWT_HEADER);
    }

    private function getPureJwt(): string
    {
        $jwtHeader = $this->getJwtHeader();
        return mb_strcut($jwtHeader, strlen(self::JWT_BEARER_NAME) + 1);
    }

    public function guest()
    {
        return !$this->check();
    }

    public function user()
    {
        return $this->user ?? null;
    }

    public function id()
    {
        return $this->user()->getAuthIdentifierName();
    }

    public function validate(array $credentials = [])
    {
    }

    public function setUser(Authenticatable $user)
    {
        $this->user = $user;
    }
}
