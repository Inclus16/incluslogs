<?php


namespace App\Services\Security;

use App\Services\Security\Abstractions\HashServiceInterface;
use Illuminate\Support\Facades\Hash;

class BcryptHashService implements HashServiceInterface
{
    public function hashString(string $string): string
    {
        return Hash::make($string);
    }

    public function verifyString(string $plain, string $hashed): bool
    {
        return Hash::check($plain, $hashed);
    }
}
