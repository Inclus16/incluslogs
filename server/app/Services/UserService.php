<?php


namespace App\Services;

use App\Dto\UserDto;
use App\Dto\UserDtoWithPassword;
use App\Models\Role;
use App\Models\User;
use App\Dto\AuthenticationResultDto;
use App\Dto\CredentialsDto;
use App\Services\Security\Abstractions\HashServiceInterface;
use App\Services\Security\JwtService;
use Ds\Vector;

class UserService
{
    private HashServiceInterface $hashService;

    private JwtService $jwtService;

    public function __construct(HashServiceInterface $hashService, JwtService $jwtService)
    {
        $this->hashService = $hashService;
        $this->jwtService = $jwtService;
    }

    public static array $addValidationRules =
        [
            'login' => 'required|unique:' . User::class . ',login|max:100',
            'password' => 'required',
            'name' => 'required|max:100',
            'role_id' => 'required|exists:' . Role::class . ',id'
        ];

    public function createUser(UserDtoWithPassword $dto): void
    {
        User::createFromDto($dto->changePassword($this->hashService->hashString($dto->getPassword())))->save();
    }

    public function authenticateUser(CredentialsDto $credentials): AuthenticationResultDto
    {
        /** @var User|null $user */
        $user = User::firstByLogin($credentials->getLogin())->first();
        if (null === $user || !$this->hashService->verifyString($credentials->getPassword(), $user->password)) {
            return AuthenticationResultDto::createErrorResult();
        } else {
            return AuthenticationResultDto::createSuccessResult($this->jwtService->makeTokens($user->getAuthData(), $user->id));
        }
    }

    public function refreshJwt(string $refreshTokenString): AuthenticationResultDto
    {
        $refreshToken = $this->jwtService->getRefreshToken($refreshTokenString);
        if (null === $refreshToken) {
            return AuthenticationResultDto::createErrorResult();
        } else {
            $user = $refreshToken->user;
            return AuthenticationResultDto::createSuccessResult($this->jwtService->makeTokens($user->getAuthData(), $user->id));
        }
    }

    public function list(): Vector
    {
        return new Vector(User::all());
    }

    public function remove(User $user)
    {
        $user->delete();
    }

    public function update(User $user, UserDto $dto)
    {
        $user->fillByDto($dto)->save();
    }

    public function changePassword(User $user, string $newPassword)
    {
        $user->password = $this->hashService->hashString($newPassword);
        $user->save();
    }
}
