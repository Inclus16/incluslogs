<?php


namespace App\Services\Logs;

use App\Dto\LogTagTargetWordDto;
use App\Models\LogTagTargetWord;
use Ds\Sequence;
use Ds\Vector;

class LogTagTargetWordService
{
    public function list(): Sequence
    {
        return new Vector(LogTagTargetWord::all());
    }

    public function add(LogTagTargetWordDto $dto): void
    {
        LogTagTargetWord::createFromDto($dto)->save();
    }

    public function remove(LogTagTargetWord $model)
    {
        $model->delete();
    }

    public function update(LogTagTargetWordDto $dto, LogTagTargetWord $model)
    {
        $model->fillByDto($dto)->save();
    }
}
