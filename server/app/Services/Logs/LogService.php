<?php


namespace App\Services\Logs;

use App\Dto\LogSearchDto;
use App\Models\ExternalLog;
use Ds\Collection;
use Ds\Vector;

class LogService
{
    public function search(LogSearchDto $dto): Collection
    {
        return new Vector(ExternalLog::searchViaDto($dto)->get());
    }

    public function delete(ExternalLog $log)
    {
        $log->delete();
    }
}
