<?php


namespace App\Services\Logs;

use App\Models\LogTagColor;
use Ds\Vector;

class LogTagColorService
{
    public function add(string $color): void
    {
        (new LogTagColor(['color' => $color]))->save();
    }

    public function remove(LogTagColor $tagColor): void
    {
        $tagColor->delete();
    }

    public function list(): Vector
    {
        return new Vector(LogTagColor::all());
    }

    public function update(LogTagColor $tagColor, string $color): void
    {
        $tagColor->setAttribute('color', $color)
            ->save();
    }
}
