<?php


namespace App\Services\Logs;

use App\Dto\LogTagDto;
use App\Dto\LogTagTreeItemDto;
use App\Models\LogTag;
use Ds\Sequence;
use Ds\Vector;

class LogTagService
{
    public function save(LogTagDto $dto): void
    {
        LogTag::createFromDto($dto)->save();
    }

    public function list(): Sequence
    {
        return new Vector(LogTag::withColor()
            ->withWords()
            ->get()->toArray());
    }

    public function update(LogTag $tag, LogTagDto $dto): void
    {
        $tag->fillFromDto($dto)->save();
    }

    public function remove(LogTag $tag): void
    {
        $tag->delete();
    }

    public function getTree(): Vector
    {
        /** @var LogTag $logTag */
        $logTag = LogTag::upperMost()->withColor()->first();
        /** @var Vector $child */
        $child = $logTag->getTree()->map(function (\stdClass $data) {
            return new LogTagTreeItemDto($data->id, $data->parent_id, $data->name, [
                'id' => $data->color_id,
                'color' => $data->color
            ], $data->color_id);
        });
        $childCount = $child->count();
        for ($i = $childCount - 1; $i > 0; $i--) {
            /** @var LogTagTreeItemDto $dto */
            $dto = $child[$i];
            /** @var LogTagTreeItemDto $parent */
            $parent = $child->filter(function (LogTagTreeItemDto $parent) use ($dto) {
                return $parent->getId() === $dto->getParentId();
            })->first();
            $parent->appendChild($dto);
            $child->remove($i);
        }
        return $child;
    }
}
