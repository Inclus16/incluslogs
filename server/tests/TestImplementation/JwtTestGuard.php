<?php


namespace Tests\TestImplementation;

use App\Models\User;
use App\Services\Security\JwtGuard;

class JwtTestGuard extends JwtGuard
{
    private User $user;

    public function check()
    {
        return true;
    }

    public function setTestUser(User $user)
    {
        $this->user = $user;
    }

    public function persistUser()
    {
        $this->setUser($this->user);
    }
}
