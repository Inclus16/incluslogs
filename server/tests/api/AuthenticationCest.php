<?php

use App\Dto\CredentialsDto;
use App\Models\RefreshToken;
use Database\Seeders\UserSeeder;
use Ds\Map;
use Ds\Vector;
use Symfony\Component\HttpFoundation\Response;
use function PHPUnit\Framework\assertNull;

class AuthenticationCest
{
    public function _before(ApiTester $I)
    {
    }

    public function failedAuthTest(ApiTester $I)
    {
        $I->wantTo('Failed authentication when credentials does not exists');
        $credentials = new CredentialsDto(uniqid(), uniqid());
        $I->sendGet(route('token.get'), $credentials->toArray());
        $I->seeErrorDefaultResponseJsonSchema();
        $I->seeResponseContainsJson(['code' => Response::HTTP_UNAUTHORIZED]);
    }

    public function successAuthTest(ApiTester $I)
    {
        $I->wantTo('Successfully get jwt tokens for all users in database');
        $credentialsVector = new Vector();
        UserSeeder::getUsersVector()
            ->apply(fn (Map $map) => $credentialsVector->push(new CredentialsDto($map->get('login'), $map->get('password'))));
        foreach ($credentialsVector as $credentials) {
            $I->sendGet(route('token.get'), $credentials->toArray());
            $I->seeSuccessDefaultResponseJsonSchema();
            $I->seeResponseJsonMatchesXpath('//data/token');
            $I->seeResponseJsonMatchesXpath('//data/refresh_token');
        }
    }

    public function successRefreshToken(ApiTester $I)
    {
        $I->wantTo('Refresh jwt via refresh token');
        /** @var RefreshToken $refreshToken */
        $refreshToken = RefreshToken::all()->last();
        $I->sendGet(route('token.refresh'), ['refresh_token' => $refreshToken->refresh_token]);
        $I->seeSuccessDefaultResponseJsonSchema();
        $I->seeResponseJsonMatchesXpath('//data/token');
        $I->seeResponseJsonMatchesXpath('//data/refresh_token');
        assertNull(RefreshToken::findByRefreshToken($refreshToken->refresh_token)->first());
    }

    public function failedRefreshToken(ApiTester $I)
    {
        $I->wantTo('Fail refresh jwt via refresh token');
        $refreshTokenString = uniqid('_', true);
        assertNull(RefreshToken::findByRefreshToken($refreshTokenString)->first());
        $I->sendGet(route('token.refresh'), ['refresh_token' => uniqid('_', true)]);
        $I->seeErrorDefaultResponseJsonSchema();
        $I->seeResponseContainsJson(['code' => Response::HTTP_UNAUTHORIZED]);
    }


    public function invalidToken(ApiTester $I)
    {
        $I->wantTo('Failed auth on invalid token');
        $I->amBearerAuthenticated(uniqid());
        $I->sendGet(route('log.tag.get'));
        $I->seeErrorDefaultResponseJsonSchema();
        $I->seeResponseContainsJson(['code' => Response::HTTP_UNAUTHORIZED]);
    }
}
