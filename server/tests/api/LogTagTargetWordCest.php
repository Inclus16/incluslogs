<?php

use App\Models\LogTag;
use App\Models\LogTagTargetWord;
use function PHPUnit\Framework\assertCount;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertNotEquals;
use function PHPUnit\Framework\assertNull;
use function PHPUnit\Framework\assertTrue;

class LogTagTargetWordCest
{
    public function _before(ApiTester $I)
    {
        $I->useDefaultUserForAuthentication();
    }

    // tests
    public function add(ApiTester $I)
    {
        $I->wantTo('Add new target word');
        /** @var LogTag $tagBeforeAddWords */
        $tagBeforeAddWords = LogTag::withWords()->latest('id')->first();
        $wordsCountBeforeAdd = $tagBeforeAddWords->words->count();
        $newWordText = uniqid();
        $I->sendPost(route('log.tag.word.post'), ['tag_id' => $tagBeforeAddWords->id, 'words' => $newWordText]);
        $I->seeSuccessDefaultResponseJsonSchema();
        /** @var LogTag $tagAfterAddWords */
        $tagAfterAddWords = LogTag::withWords()->find($tagBeforeAddWords->id);
        assertCount($wordsCountBeforeAdd + 1, $tagAfterAddWords->words);
        $lastWord = $tagAfterAddWords->words->last();
        assertEquals($newWordText, $lastWord->words);
        assertNull($tagBeforeAddWords->words->first(fn (LogTagTargetWord $word) => $word->words === $lastWord->words));
    }

    public function remove(ApiTester $I)
    {
        $I->wantTo('Remove one target word');
        /** @var LogTag $tagBeforeRemoveWords */
        $tagBeforeRemoveWords = LogTag::withWords()->latest('id')->first();
        $wordToDelete = $tagBeforeRemoveWords->words->last();
        $I->sendDelete(route('log.tag.word.delete', $wordToDelete->id));
        $I->seeSuccessDefaultResponseJsonSchema();
        /** @var LogTag $tagAfterRemoveWords */
        $tagAfterRemoveWords = LogTag::withWords()->latest('id')->first();
        assertEquals($tagBeforeRemoveWords->words->count() - 1, $tagAfterRemoveWords->words->count());
        assertNull($tagAfterRemoveWords->words->first(fn (LogTagTargetWord $word) => $word->words === $wordToDelete->words));
    }

    public function update(ApiTester $I)
    {
        $I->wantTo('Update one target word');
        /** @var LogTagTargetWord $wordToUpdate */
        $wordToUpdate = LogTagTargetWord::query()->latest('id')->first();
        $newWordText = uniqid();
        $I->sendPut(route('log.tag.word.put', $wordToUpdate->id), ['tag_id' => $wordToUpdate->tag_id, 'words' => $newWordText]);
        $I->seeSuccessDefaultResponseJsonSchema();
        /** @var LogTagTargetWord $wordAfterUpdate */
        $wordAfterUpdate = LogTagTargetWord::query()->find($wordToUpdate->id);
        assertNotEquals($wordAfterUpdate->words, $wordToUpdate->words);
        assertEquals($wordAfterUpdate->words, $newWordText);
    }

    public function list(ApiTester $I)
    {
        $I->wantTo('See all target words of specified tag');
        $I->sendGet(route('log.tag.word.get'));
        $I->seeSuccessDefaultResponseJsonSchema();
        $data = $I->grabUpperLevelSectionFromJsonResponse('data');
        $words = LogTagTargetWord::all();
        assertCount($words->count(), $data);
        for ($i = 0; $i < $words->count(); $i++) {
            assertEquals($words[$i]->id, $data[$i]['id']);
            assertEquals($words[$i]->words, $data[$i]['words']);
            assertEquals($words[$i]->tag_id, $data[$i]['tag_id']);
        }
    }
}
