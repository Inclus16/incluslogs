<?php

use App\Models\Role;
use App\Models\User;
use Database\Seeders\UserSeeder;
use Ds\Map;
use Ds\Vector;
use Illuminate\Support\Facades\DB;
use function PHPUnit\Framework\assertCount;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertNotEquals;
use function PHPUnit\Framework\assertNotNull;
use function PHPUnit\Framework\assertNull;

class UserCest
{
    public function _before(ApiTester $I)
    {
        $I->useDefaultUserForAuthentication();
    }


    public function list(ApiTester $I)
    {
        $I->wantTo('See all users');
        /** @var Vector|Map[] $users */
        $users = UserSeeder::getUsersVector();
        $I->sendGet(route('user.get'));
        $I->seeSuccessDefaultResponseJsonSchema();
        $data = $I->grabUpperLevelSectionFromJsonResponse('data');
        $usersCount = $users->count();
        assertCount($usersCount, $data);
        for ($i = 0; $i < $usersCount; $i++) {
            assertEquals($users[$i]->get('login'), $data[$i]['login']);
        }
    }

    public function add(ApiTester $I)
    {
        $I->wantTo('Add new user');
        $login = uniqid();
        $password = uniqid();
        $name = uniqid();
        $roleId = Role::WORKER_ID;
        $usersBeforeAdd = User::all();
        $I->sendPost(route('user.post'), [
            'name' => $name,
            'login' => $login,
            'password' => $password,
            'role_id' => $roleId
        ]);
        $I->seeSuccessDefaultResponseJsonSchema();
        $usersAfterAdd = User::all();
        assertCount($usersBeforeAdd->count() + 1, $usersAfterAdd);
        assertNotNull($usersAfterAdd->first(fn (User $user) => $user->login === $login
            && $user->role_id === $roleId
            && $user->name === $name));
    }

    public function remove(ApiTester $I)
    {
        $I->wantTo('Remove user');
        /** @var User $userToRemove */
        $userToRemove = User::workers()->first();
        $usersBeforeRemove = User::all();
        $I->sendDelete(route('user.delete', $userToRemove->id));
        $I->seeSuccessDefaultResponseJsonSchema();
        $usersAfterRemove = User::all();
        assertCount($usersAfterRemove->count() + 1, $usersBeforeRemove);
        assertNull($usersAfterRemove->find($userToRemove->id));
    }

    public function update(ApiTester $I)
    {
        $I->wantTo('Update user');
        /** @var User $userToUpdate */
        $userToUpdate = User::workers()->first();
        $usersBeforeUpdate = User::all();
        $name = uniqid();
        $login = uniqid();
        $I->sendPut(route('user.put', $userToUpdate->id), [
            'login' => $login,
            'name' => $name,
            'role_id' => $userToUpdate->role_id
        ]);
        $I->seeSuccessDefaultResponseJsonSchema();
        $usersAfterUpdate = User::all();
        assertCount($usersAfterUpdate->count(), $usersBeforeUpdate);
        /** @var User $userAfterUpdate */
        $userAfterUpdate = User::workers()->first();
        assertEquals($userAfterUpdate->name, $name);
        assertEquals($userAfterUpdate->login, $login);
        assertEquals($userAfterUpdate->role_id, $userToUpdate->role_id);
    }

    public function changePassword(ApiTester $I)
    {
        $I->wantTo('Change user password');
        /** @var User $userToUpdatePassword */
        $userToUpdatePassword = User::workers()->first();
        $lastPassword = DB::select('SELECT password FROM users WHERE id=:id', [':id' => $userToUpdatePassword->id])[0];
        $I->sendPatch(route('user.patch', $userToUpdatePassword->id), [
            'new_password' => uniqid()
        ]);
        $I->seeSuccessDefaultResponseJsonSchema();
        assertNotEquals($lastPassword, DB::select('SELECT password FROM users WHERE id=:id', [':id' => $userToUpdatePassword->id])[0]);
    }
}
