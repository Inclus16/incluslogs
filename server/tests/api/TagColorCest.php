<?php

use App\Models\LogTagColor;
use Illuminate\Contracts\Auth\Guard;
use Tests\TestImplementation\JwtTestGuard;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertNull;

class TagColorCest
{
    public function _before(ApiTester $I)
    {
        $I->useDefaultUserForAuthentication();
    }

    public function listTest(ApiTester $I)
    {
        $I->wantTo('See all colors');
        $I->sendGet(route('log.tag.color.get'));
        $I->seeSuccessDefaultResponseJsonSchema();
        $colors = LogTagColor::all();
        for ($i = 0; $i < $colors->count(); $i++) {
            $I->seeResponseContainsJson(['color' => $colors[$i]->color]);
            $I->seeResponseContainsJson(['id' => $colors[$i]->id]);
        }
    }

    public function addTest(ApiTester $I)
    {
        $I->wantTo('Add new color');
        $colorsCount = LogTagColor::query()->count();
        $I->sendPost(route('log.tag.color.post'), ['color' => uniqid()]);
        $I->seeSuccessDefaultResponseJsonSchema();
        assertEquals(LogTagColor::query()->count() - 1, $colorsCount);
    }

    public function removeTest(ApiTester $I)
    {
        $I->wantTo('Remove color');
        $colors = LogTagColor::all();
        $lastId = $colors->last()->id;
        $I->sendDelete(route('log.tag.color.delete', $lastId));
        $I->seeSuccessDefaultResponseJsonSchema();
        $colorsAfterDelete = LogTagColor::all();
        assertNull($colorsAfterDelete->first(fn (LogTagColor $color) => $color->id === $lastId));
    }

    public function updateTest(ApiTester $I)
    {
        $I->wantTo('Update color');
        $colors = LogTagColor::all();
        $colorToUpdate = $colors->last();
        $colorToUpdate->color = uniqid();
        $I->sendPut(route('log.tag.color.put', $colorToUpdate->id), ['color' => $colorToUpdate->color]);
        $I->seeSuccessDefaultResponseJsonSchema();
        $colorsAfterUpdate = LogTagColor::all();
        assertEquals($colors->count(), $colorsAfterUpdate->count());
        assertEquals($colorToUpdate->color, $colorsAfterUpdate->last()->color);
    }
}
