<?php

use App\Models\ExternalLog;
use App\Models\LogTag;
use Carbon\Carbon;
use function PHPUnit\Framework\assertCount;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertGreaterThanOrEqual;
use function PHPUnit\Framework\assertLessThanOrEqual;
use function PHPUnit\Framework\assertNull;
use function PHPUnit\Framework\assertTrue;

class LogsCest
{
    public function _before(ApiTester $I)
    {
        $I->useDefaultUserForAuthentication();
    }

    public function list(ApiTester $I)
    {
        $I->wantTo('List all logs');
        $I->sendGet(route('log.search.get'));
        $I->seeSuccessDefaultResponseJsonSchema();
        $data = $I->grabUpperLevelSectionFromJsonResponse('data');
        $logs = ExternalLog::all();
        assertCount($logs->count(), $data);
    }

    public function globalSearchByWords(ApiTester $I)
    {
        $I->wantTo('Search logs by words around all tags');
        $log = ExternalLog::query()->first();
        $I->sendGet(route('log.search.get'), [
            'word' => $log->body
        ]);
        $I->seeSuccessDefaultResponseJsonSchema();
        $data = $I->grabUpperLevelSectionFromJsonResponse('data');
        assertCount(1, $data);
    }

    public function globalSearchByDate(ApiTester $I)
    {
        $I->wantTo('Search logs by date around all tags');
        $dateStart = Carbon::now()->addMinutes(-2)->format('Y-m-d H:i:s');
        $dateEnd = Carbon::now()->addMinutes(-1)->format('Y-m-d H:i:s');
        $I->sendGet(route('log.search.get'), [
            'date_start' => $dateStart,
            'date_end' => $dateEnd
        ]);
        $I->seeSuccessDefaultResponseJsonSchema();
        $data = $I->grabUpperLevelSectionFromJsonResponse('data');
        foreach ($data as $log) {
            assertGreaterThanOrEqual(Carbon::parse($dateStart)->timestamp, Carbon::parse($log['created_at'])->timestamp);
            assertLessThanOrEqual(Carbon::parse($dateEnd)->timestamp, Carbon::parse($log['created_at'])->timestamp);
        }
    }

    public function searchByTag(ApiTester $I)
    {
        $I->wantTo('Search logs by tag');
        /** @var LogTag $tag */
        $tag = LogTag::withLogs()->first();
        $I->sendGet(route('log.search.get'), [
            'tag_id' => $tag->id
        ]);
        $logs = $tag->logs;
        $I->seeSuccessDefaultResponseJsonSchema();
        $data = $I->grabUpperLevelSectionFromJsonResponse('data');
        assertCount($logs->count(), $data);
        $count = count($data);
        for ($i = 0; $i < $count; $i++) {
            assertEquals($data[$i]['tag_id'], $logs[$i]->tag_id);
            assertEquals($data[$i]['id'], $logs[$i]->id);
            assertEquals($data[$i]['body'], $logs[$i]->body);
        }
    }

    public function remove(ApiTester $I)
    {
        $I->wantTo('Remove log by id');
        $logsBeforeDelete = ExternalLog::all();
        /** @var ExternalLog $logToDelete */
        $logToDelete = $logsBeforeDelete->first;
        $I->sendDelete(route('log.delete', $logToDelete->id));
        $I->seeSuccessDefaultResponseJsonSchema();
        $logsAfterDelete = ExternalLog::all();
        assertCount($logsBeforeDelete->count() - 1, $logsAfterDelete);
        assertNull($logsAfterDelete->first(fn (ExternalLog $log) => $log->body === $logToDelete->body));
    }
}
