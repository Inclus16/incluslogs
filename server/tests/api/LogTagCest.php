<?php

use App\Models\LogTag;
use App\Models\LogTagColor;
use App\Models\LogTagTargetWord;
use Ds\Vector;
use Illuminate\Contracts\Auth\Guard;
use Tests\TestImplementation\JwtTestGuard;
use function PHPUnit\Framework\assertCount;
use function PHPUnit\Framework\assertEmpty;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertNotEquals;
use function PHPUnit\Framework\assertNotNull;
use function PHPUnit\Framework\assertNull;
use function PHPUnit\Framework\assertSame;
use function PHPUnit\Framework\assertTrue;

class LogTagCest
{
    private const CREATED_NAME = 'TEST_CREATED_NAME_BLA_BLA';

    public function _before(ApiTester $I)
    {
        $I->useDefaultUserForAuthentication();
    }

    public function list(ApiTester $I)
    {
        $I->wantTo('See all tags');
        $I->sendGet(route('log.tag.get'));
        $I->seeSuccessDefaultResponseJsonSchema();
        $data = new Vector($I->grabUpperLevelSectionFromJsonResponse('data'));
        $logTags = new Vector(LogTag::withColor()->withWords()->get()->toArray());
        $logTagsCount = count($logTags);
        assertCount($logTagsCount, $data);
        for ($i = 0; $i < $logTagsCount; $i++) {
            assertEquals($logTags[$i]['id'], $data[$i]['id']);
            assertEquals($logTags[$i]['name'], $data[$i]['name']);
            assertEquals($logTags[$i]['color_id'], $data[$i]['color_id']);
            assertEquals($logTags[$i]['color'], $data[$i]['color']);
            assertEquals($logTags[$i]['words'], $data[$i]['words']);
        }
    }

    public function add(ApiTester $I)
    {
        $I->wantTo('Create new tag');
        $logTagsCount = LogTag::query()->count();
        /** @var LogTagColor $color */
        $color = LogTagColor::query()->latest('id')->first();
        $I->sendPost(route('log.tag.post'), ['name' => self::CREATED_NAME, 'color_id' => $color->id, 'parent_id' => null]);
        $I->seeSuccessDefaultResponseJsonSchema();
        assertEquals($logTagsCount + 1, LogTag::query()->count());
    }

    public function update(ApiTester $I)
    {
        $I->wantTo('Update tag');
        $logTagsCount = LogTag::query()->count();
        /** @var LogTag $logTag */
        $logTag = LogTag::query()->latest('id')->first();
        /** @var LogTagColor $color */
        $color = LogTagColor::query()->where('id', '!=', $logTag->color_id)->first();
        assertNotEquals($logTag->name, self::CREATED_NAME);
        $I->sendPut(route('log.tag.put', $logTag->id), ['name' => self::CREATED_NAME, 'color_id' => $color->id, 'parent_id' => null]);
        $I->seeSuccessDefaultResponseJsonSchema();
        /** @var LogTag $logTagAfterUpdate */
        $logTagAfterUpdate = LogTag::query()->find($logTag->id);
        assertEquals($logTagsCount, LogTag::query()->count());
        assertEquals($logTagAfterUpdate->color_id, $color->id);
        assertEquals($logTagAfterUpdate->color_id, $color->id);
        assertEquals($logTagAfterUpdate->name, self::CREATED_NAME);
        assertEquals($logTagAfterUpdate->id, $logTag->id);
        assertEquals($logTagAfterUpdate->words, $logTag->words);
    }

    public function remove(ApiTester $I)
    {
        $I->wantTo('Remove tag, all related target words, and dont remove related colors');
        /** @var LogTag $logTag */
        $logTag = LogTag::query()->latest('id')->first();
        $I->sendDelete(route('log.tag.delete', $logTag->id));
        $I->seeSuccessDefaultResponseJsonSchema();
        assertNull(LogTag::query()->find($logTag->id));
        assertNotNull(LogTagColor::query()->find($logTag->color_id));
        assertTrue(LogTagTargetWord::query()->where(['tag_id' => $logTag->id])->get()->isEmpty());
    }

    public function tree(ApiTester $I)
    {
        $I->wantTo('See tree of tags');
        $I->sendGet(route('log.tag.get.tree'));
        $I->seeSuccessDefaultResponseJsonSchema();
        $data = $I->grabUpperLevelSectionFromJsonResponse('data');
        assertCount(1, $data);
        assertCount(1, $data[0]['child']);
        assertCount(1, $data[0]['child'][0]['child']);
        assertCount(0, $data[0]['child'][0]['child'][0]['child']);
    }
}
