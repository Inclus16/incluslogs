<?php


use Symfony\Component\HttpFoundation\Response;
use function PHPUnit\Framework\assertEquals;

class AuthorizationCest
{
    public function _before(ApiTester $I)
    {
    }


    public function successAuthorizationTest(ApiTester $I)
    {
        $I->wantTo('Success authorization');
        $I->useAdminForAuthentication();
        $I->sendGet(route('user.get'));
        $I->seeSuccessDefaultResponseJsonSchema();
    }

    public function failedAuthorizationTest(ApiTester $I)
    {
        $I->wantTo('Fail authorization');
        $I->useWorkerForAuthentication();
        $I->sendGet(route('user.get'));
        $I->seeErrorDefaultResponseJsonSchema();
        $meta = $I->grabUpperLevelSectionFromJsonResponse('meta');
        assertEquals($meta['code'], Response::HTTP_FORBIDDEN);
    }
}
