<?php

use App\Models\User;
use Illuminate\Contracts\Auth\Guard;
use Tests\TestImplementation\JwtTestGuard;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause()
 *
 * @SuppressWarnings(PHPMD)
 */
class ApiTester extends \Codeception\Actor
{
    use _generated\ApiTesterActions;

    public function seeSuccessDefaultResponseJsonSchema()
    {
        $this->seeResponseCodeIs(200);
        $this->seeResponseMatchesJsonType([
            'success' => 'boolean',
            'meta' => ['code' => 'integer'],
            'data' => 'array'
        ]);
    }

    public function seeErrorDefaultResponseJsonSchema()
    {
        $this->seeResponseCodeIs(200);
        $this->seeResponseMatchesJsonType([
            'success' => 'boolean',
            'meta' => ['code' => 'integer'],
            'errors' => 'array',
        ]);
    }

    public function useDefaultUserForAuthentication()
    {
        $this->useAdminForAuthentication();
    }

    private function useTestGuard()
    {
        app()->singleton(Guard::class, JwtTestGuard::class);
    }

    private function getGuard()
    {
        return app()->get(Guard::class);
    }

    public function useAdminForAuthentication()
    {
        $this->useTestGuard();
        $this->getGuard()->setTestUser(User::administrators()->first());
    }

    public function useWorkerForAuthentication()
    {
        $this->useTestGuard();
        $this->getGuard()->setTestUser(User::workers()->first());
    }

    public function grabUpperLevelSectionFromJsonResponse(string $sectionName)
    {
        $data = json_decode($this->grabResponse(), true);
        return $data[$sectionName];
    }
}
