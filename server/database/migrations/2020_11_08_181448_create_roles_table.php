<?php

use App\Models\Role;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100)->unique();
            $table->string('description', 100);
        });
        Schema::table('users', function (Blueprint $table) {
            $table->integer('role_id')->nullable();
            $table->foreign('role_id')->references('id')->on('roles')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });
        (new Role(['id' => 1, 'name' => 'Administrator', 'description' => 'Can watch users,tags,logs,words and modify/remove them']))->save();
        (new Role(['id' => 2, 'name' => 'Worker', 'description' => 'Can watch tags,logs,words']))->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropColumns('users', ['role_id']);
        Schema::dropIfExists('roles');
    }
}
