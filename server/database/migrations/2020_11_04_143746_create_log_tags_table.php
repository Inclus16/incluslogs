<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_tags', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('parent_id')->nullable(true);
            $table->foreign('parent_id')
                ->on('log_tags')->references('id')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('external_logs', function (Blueprint $table) {
            $table->integer('tag_id');
            $table->foreign('tag_id')
               ->on('log_tags')->references('id')
               ->onDelete('cascade')
               ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropColumns('external_logs', ['tag_id']);
        Schema::dropIfExists('log_tags');
    }
}
