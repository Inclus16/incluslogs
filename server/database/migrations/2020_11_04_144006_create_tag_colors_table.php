<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTagColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_tag_colors', function (Blueprint $table) {
            $table->id();
            $table->string('color')->unique();
        });
        Schema::table('log_tags', function (Blueprint $table) {
            $table->integer('color_id');
            $table->foreign('color_id')
                ->on('log_tag_colors')->references('id')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropColumns('log_tags', ['color_id']);
        Schema::dropIfExists('log_tag_colors');
    }
}
