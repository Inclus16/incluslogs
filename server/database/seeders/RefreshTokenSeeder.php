<?php

namespace Database\Seeders;

use App\Models\RefreshToken;
use App\Models\User;
use Illuminate\Database\Seeder;

class RefreshTokenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new RefreshToken(['refresh_token' => uniqid('_', true),
            'user_id' => User::query()->latest('created_at')->first()->id]))->save();
    }
}
