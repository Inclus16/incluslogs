<?php

namespace Database\Seeders;

use App\Dto\UserDtoWithPassword;
use App\Models\Role;
use App\Services\UserService;
use Ds\Map;
use Ds\Vector;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    private UserService $userService;


    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        self::getUsersVector()->apply(fn (Map $map) => $this->userService->createUser(
            new UserDtoWithPassword(
            $map->get('login'),
            $map->get('password'),
            $map->get('name'),
            $map->get('roleId')
        )
        ));
    }

    public static function getUsersVector(): Vector
    {
        return new Vector([
            new Map([
                'login' => 'test',
                'password' => 'test',
                'name' => 'test',
                'roleId' => Role::ADMINISTRATOR_ID
            ]),
            new Map([
                'login' => 'test2',
                'password' => 'test2',
                'name' => 'test2',
                'roleId' => Role::WORKER_ID
            ])
        ]);
    }
}
