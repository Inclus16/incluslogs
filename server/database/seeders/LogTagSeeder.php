<?php

namespace Database\Seeders;

use App\Models\LogTag;
use App\Models\LogTagColor;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Seeder;

class LogTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $colors = LogTagColor::all();
        /** @var LogTag|null $lastTag */
        $lastTag = null;
        $colors->each(function (LogTagColor $color) use (&$lastTag) {
            $tag = new LogTag();
            $tag->color_id = $color->id;
            $tag->name = uniqid();
            if (null !== $lastTag) {
                $tag->parent_id = $lastTag->id;
            }
            $tag->save();
            $lastTag = $tag;
        });
        unset($lastTag);
    }
}
