<?php

namespace Database\Seeders;

use App\Models\LogTagColor;
use Ds\Vector;
use Illuminate\Database\Seeder;

class TagColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        self::getColors()->apply(fn (string $color) => (new LogTagColor(['color' => $color]))->save());
    }

    public static function getColors(): Vector
    {
        return new Vector(['black', 'red', 'purple']);
    }
}
