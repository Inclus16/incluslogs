<?php

namespace Database\Seeders;

use App\Models\LogTag;
use App\Models\LogTagTargetWord;
use Illuminate\Database\Seeder;

class LogTagTargetWordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = LogTag::all();
        $tags->each(function (LogTag $logTag) {
            for ($i = 0; $i < 3; $i++) {
                $word = new LogTagTargetWord();
                $word->words = uniqid();
                $word->tag_id = $logTag->id;
                $word->save();
            }
        });
    }
}
