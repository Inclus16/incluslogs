<?php

namespace Database\Seeders;

use App\Models\ExternalLog;
use App\Models\LogTagTargetWord;
use Carbon\Carbon;
use Ds\Collection;
use Ds\Vector;
use Illuminate\Database\Seeder;

class ExternalLogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->getAllWords()->apply(function (LogTagTargetWord $word) {
            for ($i = 0; $i < 3; $i++) {
                $log = new ExternalLog(['tag_id' => $word->tag_id, 'body' => uniqid('') . $word->words . uniqid('')]);
                $log->created_at = Carbon::now()->addMinutes(-$i)->format('Y-m-d H:i:s');
                $log->save();
            }
        });
    }

    public function getAllWords(): Vector
    {
        return new Vector(LogTagTargetWord::all());
    }
}
